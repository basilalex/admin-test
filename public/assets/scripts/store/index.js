import { createStore, compose } from "redux";
import leads from './leads';
import statistic from './statistic';

const initialState = {
  leads,
  statistic,
  stats: {
    totalLeads: 120,
    totalLeadsPercentage: "+10%",
    totalEmailsOpened: 100,
    totalEmailsOpenedPercentage: "+9%",
    emailOpenRate: "88%",
    emailOpenRatePercentage: "+8.9%"
  },
  connectedAccounts: {
    connectedGmailAccounts: [],
    connectedFacebookAccounts: []
  },
  emailTemplates: {
    connectedGmailAccountId: "",
    connectedFacebookAccountId: "",
    connectedFacebookFormId: "",
    behaviour: "",
    createdOn: "",
    noOfEmailsSent: 10,
    noOfEmailsOpened: 7
  }
};

function rootReducer(state = initialState, action) {
  return state;
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers());

export default store;
