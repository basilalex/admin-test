const statistic = [
  { title: 'Total Leads', data: [ 0, 5, 10, 13, 9, 8 ], color: '#4caf50', total: 2, totalPercentage: 1 },
  { title: 'Total Emails Opened', data: [ 0, 5, 10, 13, 9, 8 ], color: '#9675ce', total: 2, totalPercentage: 1 },
  { title: 'Email Open Rate', data: [ 0, 5, 10, 13, 9, 8 ], color: '#f96262', total: 2, totalPercentage: 1 },
];

export default statistic;
