const leads = {
  title: 'Leads Report',
  overview: 'Overview',
  date: 'February 2019',
  color: '#00a9f4',
  data: [
    { name: 'Name', status: 4, date: 'Nov 18' },
    { name: 'Name', status: 1, date: 'Nov 19' },
    { name: 'Name', status: 2, date: 'Nov 20' },
    { name: 'Name', status: 3, date: 'Nov 21' },
  ]
};

export default leads;
