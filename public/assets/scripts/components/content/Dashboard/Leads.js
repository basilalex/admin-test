import React from 'react';
import Badge from './Badge';

const TableHeader = ({ names }) => (
  <thead>
    <tr>
      {names.map(name => <th key={name} className="bdwT-0">{name}</th>)}
    </tr>
  </thead>
);

const TableBody = ({ data }) => (
  <tbody>
    {data
      ? data.map(({ name, status, date }, id) => (
        <tr key={id}>
          <td className="fw-600">{name}</td>
          <td><Badge status={status} /></td>
          <td>{date}</td>
        </tr>
      )) : 'Empty'}
  </tbody>
);

const Leads = ({ title, overview, date, color, data }) => (
  <div className="bd bgc-white">
    <div className="layers">
      <div className="layer w-100 p-20">
        <h6 className="lh-1">{title}</h6>
      </div>

      <div className="layer w-100">
        <div className="c-white p-20" style={{background: color}}>
          <div className="peers ai-c jc-sb gap-40">
            <div className="peer peer-greed">
              <h5>{date}</h5>
              <p className="mB-0">{overview}</p>
            </div>
            <div className="peer">
              <h3 className="text-right">{data ? data.length : 0} Leads</h3>
            </div>
          </div>
        </div>
        <div className="table-responsive p-20">
          <table className="table">
            <TableHeader names={['Name', 'Status', 'Date']} />
            <TableBody data={data} />
          </table>
        </div>
      </div>
    </div>
  </div>
);

export default Leads;
