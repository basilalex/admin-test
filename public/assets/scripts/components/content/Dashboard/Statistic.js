import React, { PureComponent } from 'react';

class Statistic extends PureComponent {

  componentDidMount() {
    const { color, data, id } = this.props;
    $(`#${id}`).sparkline(data, {
      type: 'bar',
      height: '20',
      barWidth: '3',
      resize: true,
      barSpacing: '3',
      barColor: color,
    });
  }

  render() {
    const { title, color, total, totalPercentage, id } = this.props;
  
    const peerClassName = "d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15";
    const peerStyles = { color, backgroundColor: `${color}33` };

    return (
      <div className="col-md-3">
        <div className="layers bd bgc-white p-20">
          <div className="layer w-100 mB-10">
            <h6 className="lh-1">{title}</h6>
          </div>
          <div className="layer w-100">
            <div className="peers ai-sb fxw-nw">
              <div className="peer peer-greed">
                <span id={id} />
              </div>
              <div className="peer">
                <span style={peerStyles} className={peerClassName}>{total}</span>
                <span style={peerStyles} className={peerClassName}>{totalPercentage}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Statistic;
