import React from 'react';

const crateBadge = (status, color) => (
  <span className={`badge ${color} p-10 lh-0 tt-c badge-pill`}>
    {status}
  </span>
);

const badges = {
  1: crateBadge('New', 'bgc-green-50 c-green-700'),
  2: crateBadge('Used', 'bgc-deep-purple-50 c-deep-purple-700'),
  3: crateBadge('Old', 'bgc-red-50 c-red-700'),
  4: crateBadge('Unavailable', 'bgc-yellow-50 c-yellow-700'),
};

const Badge = ({ status }) => badges[status];

export default Badge;
