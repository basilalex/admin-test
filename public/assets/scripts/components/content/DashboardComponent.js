import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Leads from './Dashboard/Leads';
import Statistic from './Dashboard/Statistic';

const mapStats = (stat, id) => <Statistic key={id} id={id} {...stat} />;

class DashboardComponent extends PureComponent {
  render() {
    const { leads, statistic } = this.props;
    return (
      <main className="main-content bgc-grey-100">
        <div id="mainContent">
          <div className="row gap-20 masonry pos-r">
            <div className="masonry-sizer col-md-6" />
            <div className="masonry-item  w-100">
              <div className="row gap-20">
                {statistic && statistic.map(mapStats)}
              </div>
            </div>

            <div className="masonry-item row gap-20">
              <Leads {...leads} />
            </div>
          </div>
        </div>
      </main>
    );
  }
}

const mapStateToProps = ({ statistic, leads }) => ({ statistic, leads });

export default connect(mapStateToProps, null)(DashboardComponent);
